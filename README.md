# LeetCode Solutions

I am practicing [LeetCode](https://leetcode.com/) to learn the [Rust programming language](https://www.rust-lang.org/).
These solutions are definitely not the best, but they pass all tests.

That's one of the things I like about Rust.
It makes testing very easy simply by specifying the `#[test]` attribute and using the `assert_eq!` macro.
For example,
``` rust
fn add(x: i32, y: i32) -> i32 {
    x + y
}

#[test]
fn test() {
    assert_eq!(add(1, 2), 3);
}
```

The [rust-analyzer](https://rust-analyzer.github.io/) extension for Visual Studio Code makes running tests as easy as pressing the "Run Test" button.