//! https://leetcode.com/problems/squares-of-a-sorted-array/

pub fn sorted_squares(nums: Vec<i32>) -> Vec<i32> {
    let mut squares = nums.iter().map(|n| n.pow(2)).collect::<Vec<i32>>();
    squares.sort();
    squares
}