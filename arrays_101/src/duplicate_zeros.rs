//! https://leetcode.com/problems/duplicate-zeros/

pub fn duplicate_zeros(arr: &mut Vec<i32>) {
    let mut indices: Vec<usize> = Vec::new();
    let mut count = 0;
    for (i, n) in arr.clone().into_iter().enumerate() {
        if n == 0 {
            indices.push(i);
        }
    }
    for i in indices {
        if i + count >= arr.len() {
            break;
        }
        arr.insert(i + count, 0);
        arr.pop();
        count += 1;
    }
}