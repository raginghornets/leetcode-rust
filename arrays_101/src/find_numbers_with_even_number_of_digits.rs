//! https://leetcode.com/problems/find-numbers-with-even-number-of-digits/

pub fn find_numbers(nums: Vec<i32>) -> i32 {
    let mut count = 0;
    for n in nums {
        if even_digits(n) {
            count += 1;
        }
    }
    count
}

fn even_digits(mut n: i32) -> bool {
    let mut divisions = 0;
    while n > 0 {
        n /= 10;
        divisions += 1;
    }
    divisions % 2 == 0
}
