//! https://leetcode.com/problems/merge-sorted-array/

fn merge(nums1: &mut Vec<i32>, m: i32, nums2: &mut Vec<i32>, _n: i32) {
    let mut nums2 = nums2.into_iter();
    let m = m as usize;
    let mut counter = 0;

    loop {
        let next = nums2.next();
        let mut index1: usize = 0;

        match next {
            Some(x) => {
                let mut inserted = false;

                while index1 < m + counter {
                    if nums1.get(index1).unwrap() > x {
                        nums1.insert(index1, *x);
                        nums1.pop();
                        inserted = true;
                        counter += 1;
                        break;
                    }

                    index1 += 1;
                }

                if !inserted {
                    nums1.insert(m + counter, *x);
                    nums1.pop();
                    counter += 1;
                }
            }
            None => break,
        }
    }
}

#[test]
fn test_one() {
    let mut nums1 = vec![1, 2, 3, 0, 0, 0];
    let mut nums2 = vec![2, 5, 6];
    let m = 3;
    let n = 3;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1, 2, 2, 3, 5, 6]);
}

#[test]
fn test_two() {
    let mut nums1 = vec![1];
    let mut nums2 = vec![];
    let m = 1;
    let n = 0;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1]);
}

#[test]
fn test_three() {
    let mut nums1 = vec![0];
    let mut nums2 = vec![1];
    let m = 0;
    let n = 1;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1]);
}

#[test]
fn test_four() {
    let mut nums1 = vec![1, 0];
    let mut nums2 = vec![2];
    let m = 1;
    let n = 1;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1, 2]);
}

#[test]
fn test_five() {
    let mut nums1 = vec![2, 0];
    let mut nums2 = vec![1];
    let m = 1;
    let n = 1;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1, 2]);
}

#[test]
fn test_six() {
    let mut nums1 = vec![1, 2, 3, 0, 0, 0];
    let mut nums2 = vec![4, 5, 6];
    let m = 3;
    let n = 3;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1, 2, 3, 4, 5, 6]);
}

#[test]
fn test_seven() {
    let mut nums1 = vec![4, 0, 0, 0, 0, 0];
    let mut nums2 = vec![1, 2, 3, 5, 6];
    let m = 1;
    let n = 5;

    merge(&mut nums1, m, &mut nums2, n);

    assert_eq!(nums1, vec![1, 2, 3, 4, 5, 6]);
}