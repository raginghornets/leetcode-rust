//! https://leetcode.com/problems/max-consecutive-ones/

pub fn find_max_consecutive_ones(nums: Vec<i32>) -> i32 {
    let mut max_count = 0;
    let mut count = 0;
    for i in nums {
        if i == 0 {
            count = 0;
        } else {
            count += 1;
        }
        if count > max_count {
            max_count = count;
        }
    }
    max_count
}
