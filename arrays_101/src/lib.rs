//! https://leetcode.com/explore/learn/card/fun-with-arrays/

#![allow(dead_code)]

mod max_consecutive_ones;
mod find_numbers_with_even_number_of_digits;
mod squares_of_a_sorted_array;
mod duplicate_zeros;
mod merge_sorted_array;